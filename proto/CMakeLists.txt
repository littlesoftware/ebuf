cmake_minimum_required( VERSION 3.12 )
project( proto )

find_package( Protobuf CONFIG REQUIRED )
find_package( Protobuf REQUIRED )
file( GLOB_RECURSE PROTOS "*.proto" )
protobuf_generate_cpp( SOURCES HEADERS ${PROTOS} )

add_library( ${PROJECT_NAME} STATIC ${SOURCES} ${HEADERS} )
target_link_libraries( ${PROJECT_NAME} protobuf::libprotobuf )

