# buff

BUFFer - is project for make available save and give data from abstract buffers.

### Targets

* [ ] Add SQLite library and work with buffer in this data base
* [ ] Add ZeroMQ library and make base config of client/server conception
* [ ] Add YAML config file
* [ ] Add functional of forking server and work with him through client
* [ ] Add functional of copy to buffer
* [ ] Add functional of get from buffer
* [ ] Add functional name of buffer
* [ ] Add functional namespace of buffer
* [ ] Add default buffer as clipboard

## Build and install

```bash
git clone https://gitlab.com/littlesoftware/buff.git
cd buff
mkdir build
cd build
cmake ..
cmake --build .
sudo cmake --build . --target install
```


## License

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribate it.
There is NO WARRANTY, to the extent permitted by law.

Written by Nikolaev Anton.

