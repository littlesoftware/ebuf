#include "src/Handler.h"

int main( int argc, char** argv )
{
  return Handler::execute( argc, argv );
}
