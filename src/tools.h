#pragma once

#include <string>
#include <optional>

namespace tools
{

std::string format( const char* fmt, ... );
void createIpcDirectory();
void createCacheDirectory();
const std::string& getProtocol();
const std::string& getDefaultCacheFile();
std::optional< std::string > getConfigFile();
bool isLockServer();
int lockServer();
void unlockServer( int file );

} // namespace tools
