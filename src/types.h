#pragma once

#include <vector>
#include <string>
#include <optional>

using List = std::vector< std::string >;
using OptStr = std::optional< std::string >;

