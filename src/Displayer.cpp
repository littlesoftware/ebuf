#include "Displayer.h"

#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/file.h>

Displayer::Displayer()
{
  m_client.connect();
}

Displayer::~Displayer()
{
  m_client.close();
}

void Displayer::setBufferName( const std::string& name )
{
  m_name = name;
}

void Displayer::setBufferNamespace( const OptStr& ns )
{
  m_namespace = ns;
}

void Displayer::setBufferValue( const std::string& value )
{
  m_value.str( value );
}

void Displayer::setDefaultValue( const OptStr& value )
{
  m_defaultValue = value;
}

void Displayer::readDesc( int desc )
{
  char buffer[ 1024 ];
  while( true )
  {
    auto bytes = read( desc, buffer, 1024 );
    if( bytes < 0 )
      throw std::logic_error( strerror( errno ) );
    if( bytes > 0 )
      m_value.write( buffer, bytes );
    else
      break;
  }
}

void Displayer::readStdin()
{
  readDesc( STDIN_FILENO );
}

void Displayer::readFile( const std::string& filename )
{
  FILE* file = fopen( filename.c_str(), "rb" );
  if( !file )
    throw std::logic_error( strerror( errno ) );
  readDesc( fileno( file ) );
  fclose( file );
}

void Displayer::onKillServer()
{
  m_client.killServer();
}

void Displayer::onGetBuffer()
{
  m_value.str( m_client.getBuffer( m_name, m_namespace, m_defaultValue ) );
  std::cout << m_value.str();
}

void Displayer::onSetBuffer()
{
  m_client.setBuffer( m_name, m_namespace, m_value.str() );
}

void Displayer::onBufferList()
{
  const auto list = m_client.getBufferList( m_namespace );
  for( const auto& item : list )
    std::cout << item << std::endl;
}

void Displayer::onNamespaceList()
{
  const auto list = m_client.getNamespaceList();
  for( const auto& item : list )
    std::cout << item << std::endl;
}

void Displayer::onRemoveBuffer()
{
  m_client.removeBuffer( m_name, m_namespace );
}

void Displayer::onRemoveNamespace()
{
  if( !m_namespace.has_value() )
    throw std::logic_error( "Remove namespace required name" );
  m_client.removeNamespace( m_namespace.value() );
}

void Displayer::onRemoveAll()
{
  m_client.removeAll();
}

void Displayer::onCheckBuffer()
{
  if( !m_client.checkBuffer( m_name, m_namespace ) )
    throw 5;
}

void Displayer::onCheckNamespace()
{
  if( !m_namespace.has_value() )
    throw std::logic_error( "Check namespace required name" );
  if( !m_client.checkNamespace( m_namespace.value() ) )
    throw 5;
}

