#include "Manager.h"

#include <iostream>

Manager::Manager()
{
  m_server.setListener( this );
}

Manager::~Manager()
{
  m_storage.close();
}

void Manager::runServer()
{
  try
  {
    m_config.load();
    m_storage.open( m_config.getCacheFile() );
    m_server.setDefaultValue( m_config.getDefaultValue() );
    m_server.setDefaultNamespace( m_config.getDefaultNamespace() );
    m_server.bind();
    m_server.run();
    m_storage.close();
  }
  catch( const std::exception& error )
  {
    std::cerr << "Error: " << error.what() << std::endl;
  }
  catch( ... )
  {
    std::cerr << "Unknown error" << std::endl;
  }
  m_server.close();
}

void Manager::onKillServer()
{
  m_server.stop();
}

OptStr Manager::onGetBuffer( const std::string& name, const std::string& ns )
{
  if( !m_storage.checkBuffer( ns, name ) )
    return std::nullopt;
  return m_storage.get( ns, name );
}

void Manager::onSetBuffer( const std::string& name,
                           const std::string& ns,
                           const std::string& value )
{
  m_storage.set( ns, name, value );
}

List Manager::onGetBufferList( const std::string& ns )
{
  return m_storage.getBufferList( ns );
}

List Manager::onGetNamespaceList()
{
  return m_storage.getNamespaceList();
}

void Manager::onRemoveBuffer( const std::string& name, const std::string& ns )
{
  m_storage.removeBuffer( ns, name );
}

void Manager::onRemoveNamespace( const std::string& ns )
{
  m_storage.removeNamespace( ns );
}

void Manager::onRemoveAll()
{
  m_storage.removeAll();
}

bool Manager::onCheckBuffer( const std::string& name, const std::string& ns )
{
  return m_storage.checkBuffer( ns, name );
}

bool Manager::onCheckNamespace( const std::string& ns )
{
  return m_storage.checkNamespace( ns );
}

