#pragma once

#include "Client.h"
#include "types.h"

#include <sstream>

class Displayer
{
public:
  Displayer();
  ~Displayer();

  void setBufferName( const std::string& name );
  void setBufferNamespace( const OptStr& ns );
  void setBufferValue( const std::string& value );
  void setDefaultValue( const OptStr& value );

  void readDesc( int desc );
  void readStdin();
  void readFile( const std::string& filename );

  void onKillServer();
  void onGetBuffer();
  void onSetBuffer();
  void onBufferList();
  void onNamespaceList();
  void onRemoveBuffer();
  void onRemoveNamespace();
  void onRemoveAll();
  void onCheckBuffer();
  void onCheckNamespace();

private:
  std::string m_name;
  OptStr m_namespace;
  OptStr m_defaultValue;
  std::stringstream m_value;
  Client m_client;
};
