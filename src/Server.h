#pragma once

#include "ServerListener.h"

#include <string>

namespace reqrep
{
  class Request;
  class Response;
} // namespace reqrep

class Server
{
public:
  Server();
  ~Server();

  void bind();
  void close();
  void run();
  void stop();
  void setListener( ServerListener* listener );
  void setDefaultNamespace( const std::string& ns );
  void setDefaultValue( const std::string& value );

  void makeErrorResponse( const std::string& message );
  void makeSuccessResponse();

  void onKillServer();
  void onSetBuffer();
  void onGetBuffer();
  void onGetBufferList();
  void onGetNamespaceList();
  void onRemoveBuffer();
  void onRemoveNamespace();
  void onRemoveAll();
  void onCheckBuffer();
  void onCheckNamespace();

private:
  void* m_socket;
  reqrep::Request* m_request;
  reqrep::Response* m_response;
  ServerListener* m_listener;
  bool m_running;
  std::string m_defaultNamespace;
  std::string m_defaultValue;
};
