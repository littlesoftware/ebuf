#include "Handler.h"
#include "tools.h"
#include "Manager.h"
#include "Displayer.h"

#include <getopt.h>
#include <iostream>
#include <unistd.h>

Handler::Handler():
  m_action( Action::Unknown )
{
}

Handler::~Handler()
{
}

int Handler::execute( int argc, char** argv )
{
  Handler& handler = instance();
  try
  {
    handler.parseOptions( argc, argv );
    switch( handler.getAction() )
    {
    case Action::Kill:
      handler.onKill();
      break;
    case Action::Yank:
      handler.makeServer();
      handler.onYank();
      break;
    case Action::Paste:
      handler.makeServer();
      handler.onPaste();
      break;
    case Action::BufferList:
      handler.makeServer();
      handler.onBufferList();
      break;
    case Action::NamespaceList:
      handler.makeServer();
      handler.onNamespaceList();
      break;
    case Action::RemoveBuffer:
      handler.makeServer();
      handler.onRemoveBuffer();
      break;
    case Action::RemoveNamespace:
      handler.makeServer();
      handler.onRemoveNamespace();
      break;
    case Action::RemoveAll:
      handler.makeServer();
      handler.onRemoveAll();
      break;
    case Action::CheckBuffer:
      handler.makeServer();
      handler.onCheckBuffer();
      break;
    case Action::CheckNamespace:
      handler.makeServer();
      handler.onCheckNamespace();
      break;
    case Action::Help:
      handler.onHelp();
      break;
    case Action::Version:
      handler.onVersion();
      break;
    default:
      std::cerr << "Unknown action" << std::endl;
      return 1;
    }
  }
  catch( const std::exception& err )
  {
    std::cerr << "Error: " << err.what() << std::endl;
    return 2;
  }
  catch( int code )
  {
    return code;
  }
  catch( ... )
  {
    std::cerr << "Unknown error" << std::endl;
    return 3;
  }
  return 0;
}

Handler& Handler::instance()
{
  static Handler obj;
  return obj;
}

void Handler::parseOptions( int argc, char** argv )
{
  const option long_options[] = {
    { "help", no_argument, nullptr, 'h' },
    { "version", no_argument, nullptr, 'v' },
    { "kill", no_argument, nullptr, 'k' },
    { "remove-buffer", no_argument, nullptr, 'r' },
    { "remove", no_argument, nullptr, 'r' },
    { "remove-namespace", no_argument, nullptr, 'R' },
    { "remove-all", no_argument, nullptr, 'D' },
    { "buffer-list", no_argument, nullptr, 'l' },
    { "list", no_argument, nullptr, 'l' },
    { "namespace-list", no_argument, nullptr, 'L' },
    { "check-buffer", no_argument, nullptr, 'c' },
    { "check", no_argument, nullptr, 'c' },
    { "check-namespace", no_argument, nullptr, 'C' },
    { "yank", required_argument, nullptr, 'y' },
    { "copy", required_argument, nullptr, 'y' },
    { "namespace", required_argument, nullptr, 'n' },
    { "default", required_argument, nullptr, 'd' },
    { nullptr, 0, nullptr, 0 },
  };
  const char short_options[] = "hvkrRlLcCy:n:d:";
  int name;
  int argind;
  m_action = Action::Unknown;

  while( ( name = getopt_long( argc, argv, short_options, long_options, nullptr ) ) != -1 )
  {
    switch( name )
    {
    case 'h':
      m_action = Action::Help;
      break;
    case 'v':
      m_action = Action::Version;
      break;
    case 'k':
      m_action = Action::Kill;
      break;
    case 'r':
      m_action = Action::RemoveBuffer;
      break;
    case 'R':
      m_action = Action::RemoveNamespace;
      break;
    case 'D':
      m_action = Action::RemoveAll;
      break;
    case 'l':
      m_action = Action::BufferList;
      break;
    case 'L':
      m_action = Action::NamespaceList;
      break;
    case 'c':
      m_action = Action::CheckBuffer;
      break;
    case 'C':
      m_action = Action::CheckNamespace;
      break;
    case 'y':
      m_action = Action::Yank;
      m_buffer = optarg;
      break;
    case 'n':
      m_namespace = optarg;
      break;
    case 'd':
      m_defaultValue = optarg;
      break;
    case '?':
      throw 4;
      break;
    }
  }

  argind = optind;
  for( ; argind < argc; argind++ )
  {
    m_args.push_back( argv[ argind ] );
  }

  if( m_action == Action::Unknown )
  {
    m_action = Action::Paste;
  }
}

Handler::Action Handler::getAction() const
{
  return m_action;
}

void Handler::onHelp()
{
  std::cout <<
    "Usage: buff [OPTION]... [BUFFER]..." << std::endl <<
    "Save and Paste BUFFER(s) into virtual storage" << std::endl <<
    std::endl <<
    "  -h, --help               print this help description" << std::endl <<
    "  -v, --version            print buff's version" << std::endl <<
    "  -k, --kill               kill buff's server" << std::endl <<
    "  -r, --remove-buffer" << std::endl <<
    "      --remove             remove buffer(s)" << std::endl <<
    "  -R, --remove-namespace   remove the namespace secified in the namespace" << std::endl <<
    "      --remove-all         remove all namespaces and buffers" << std::endl <<
    "  -l, --buffer-list" << std::endl <<
    "      --list               print all available buffers in the namespace" << std::endl <<
    "  -L, --namespace-list     print all available namespaces" << std::endl <<
    "  -c, --check-buffer" << std::endl <<
    "      --check              check for buffer existence" << std::endl <<
    "  -C, --check-namespace    check for namespace existence" << std::endl <<
    "  -y, --yank=BUFFER" << std::endl <<
    "      --copy=BUFFER        copy buffer into BUFFER" << std::endl <<
    "  -n, --namespace=NS       specifies the name of the namespace" << std::endl <<
    "  -d, --default            default value in case of no buffer" << std::endl <<
    std::endl <<
    "Examples:" << std::endl <<
    "  buff some        Print buffer some to output" << std::endl <<
    "  buff -y some     Copy (yank) from stdin to buffer some" << std::endl <<
    "  buff -d template some" << std::endl <<
    "                   Print buffer some, with default value template" << std::endl;
}

void Handler::onVersion()
{
  std::cout <<
    "buff 1.0" << std::endl <<
    "Copyright (c) 2023 Free Software Foundatin, Inc." << std::endl <<
    "License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>." << std::endl <<
    "This is free software: you are free to change and redistribute it." << std::endl <<
    "There is NO WARRANTY, to the extent permitted by law." << std::endl <<
    std::endl <<
    "Written by Nikolaev Anton." << std::endl;
}

void Handler::onKill()
{
  if( tools::lockServer() )
    return;
  Displayer displayer;
  displayer.onKillServer();
}

void Handler::onYank()
{
  Displayer displayer;
  displayer.setBufferName( m_buffer );
  displayer.setBufferNamespace( m_namespace );
  if( m_args.empty() )
  {
    displayer.readStdin();
  }
  else
  {
    for( const auto& filename : m_args )
      if( filename == "-" )
        displayer.readStdin();
      else
        displayer.readFile( filename );
  }
  displayer.onSetBuffer();
}

void Handler::onPaste()
{
  Displayer displayer;
  displayer.setDefaultValue( m_defaultValue );
  displayer.setBufferNamespace( m_namespace );
  for( const auto& name : m_args )
  {
    displayer.setBufferName( name );
    displayer.onGetBuffer();
  }
}

void Handler::onBufferList()
{
  Displayer displayer;
  displayer.setBufferNamespace( m_namespace );
  displayer.onBufferList();
}

void Handler::onNamespaceList()
{
  Displayer displayer;
  displayer.onNamespaceList();
}

void Handler::onRemoveBuffer()
{
  Displayer displayer;
  displayer.setBufferNamespace( m_namespace );
  for( const auto& name : m_args )
  {
    displayer.setBufferName( name );
    displayer.onRemoveBuffer();
  }
}

void Handler::onRemoveNamespace()
{
  Displayer displayer;
  displayer.setBufferNamespace( m_namespace );
  displayer.onRemoveNamespace();
}

void Handler::onRemoveAll()
{
  Displayer displayer;
  displayer.onRemoveAll();
}

void Handler::onCheckBuffer()
{
  Displayer displayer;
  displayer.setBufferNamespace( m_namespace );
  for( const auto& name : m_args )
  {
    displayer.setBufferName( name );
    displayer.onCheckBuffer();
  }
}

void Handler::onCheckNamespace()
{
  Displayer displayer;
  displayer.setBufferNamespace( m_namespace );
  displayer.onCheckNamespace();
}

void Handler::makeServer()
{
  tools::createIpcDirectory();
  if( tools::isLockServer() )
  {
    int pid;
    if( ( pid = fork() ) < 0 )
    {
      throw std::logic_error( "Cannot make fork" );
    }
    else if( pid == 0 )
    {
      onRunServer();
      exit( 0 );
    }
    // pid > 0 is client thread
  }
}

void Handler::onRunServer()
{
  int lock;
  if( ( lock = tools::lockServer() ) != 0 )
  {
    Manager manager;
    manager.runServer();
    tools::unlockServer( lock );
  }
}

