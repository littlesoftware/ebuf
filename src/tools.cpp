#include "tools.h"

#include <cstdarg>
#include <vector>
#include <string>
#include <filesystem>
#include <unistd.h>
#include <sys/file.h>
#include <errno.h>
#include <sys/types.h>
#include <pwd.h>

namespace tools
{

std::string format( const char* fmt, ... )
{
  va_list args;
  va_start( args, fmt );

  // get length
  va_list copy;
  va_copy( copy, args );
  const int length = std::vsnprintf( nullptr, 0, fmt, copy );
  va_end( copy );

  std::vector< char > data( length + 1 );
  std::vsnprintf( data.data(), data.size(), fmt, args );
  va_end( args );
  
  return std::string( data.data(), length );
}

void createIpcDirectory()
{
  static std::string path = format(
    "%s/buff-%d",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  std::filesystem::create_directory( path );
}

void createCacheDirectory()
{
  struct passwd* pw = getpwuid( getuid() );
  std::string path = format(
    "%s/.cache",
    pw->pw_dir );
  std::filesystem::create_directory( path );
}

const std::string& getProtocol()
{
  static std::string protocol = format(
    "ipc://%s/buff-%d/control",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );
  return protocol;
}

const std::string& getDefaultCacheFile()
{
  struct passwd* pw = getpwuid( getuid() );
  static std::string path = format(
    "%s/.cache/buff.sqlite3",
    pw->pw_dir );
  return path;
}

std::optional< std::string > getConfigFile()
{
  struct passwd* pw = getpwuid( getuid() );
  const std::string path = format(
    "%s/.config/buff/buff.yaml",
    pw->pw_dir );
  if( !std::filesystem::is_regular_file( path ) )
    return std::nullopt;
  return path;
}

bool isLockServer()
{
  int file;
  const std::string path = format(
    "%s/buff-%d/control.pid",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );

  file = open( path.c_str(), O_CREAT | O_RDWR, 0666 );
  if( !file )
    return false;
  if( flock( file, LOCK_EX | LOCK_NB ) )
  {
    close( file );
    return false;
  }
  flock( file, LOCK_UN );
  close( file );
  return true;
}

int lockServer()
{
  int file;
  std::string path = format(
    "%s/buff-%d/control.pid",
    std::filesystem::temp_directory_path().c_str(),
    getuid() );

  file = open( path.c_str(), O_CREAT | O_RDWR, 0666 );
  if( !file )
    return 0;
  if( flock( file, LOCK_EX | LOCK_NB ) )
  {
    close( file );
    return 0;
  }
  return file;
}

void unlockServer( int file )
{
  flock( file, LOCK_UN );
  close( file );
}

} // namespace tools
