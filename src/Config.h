#pragma once

#include <string>

class Config
{
public:
  Config();
  ~Config();

  void load();
  const std::string& getDefaultValue() const;
  const std::string& getDefaultNamespace() const;
  const std::string& getCacheFile() const;

private:
  std::string m_defaultValue;
  std::string m_defaultNamespace;
  std::string m_cacheFile;
};
