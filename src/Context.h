#pragma once

class Context
{
private:
  Context();
  ~Context();

public:
  static void* get();

private:
    void* m_context;
};
