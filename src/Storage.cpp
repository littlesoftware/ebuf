#include "Storage.h"

#include <sqlite3.h>
#include <stdexcept>
#include <assert.h>
#include <algorithm>

Storage::Storage():
  m_db( nullptr )
{
}

Storage::~Storage()
{
  close();
}

void Storage::open( const std::string& filename )
{
  close();
  if( sqlite3_open( filename.c_str(), &m_db ) )
    throw std::logic_error( sqlite3_errmsg( m_db ) );
  init();
}

void Storage::close()
{
  if( m_db )
  {
    sqlite3_close( m_db );
    m_db = nullptr;
  }
}

void Storage::set( const std::string& ns, const std::string& buffer, const std::string& value )
{
  bool isCachedNs = hasCacheNamespace( ns );
  auto& bf = getCacheNamespace( ns );
  if( !isCachedNs )
  {
    bf.nsId = getNamespaceId( ns );
  }
  insertBuffer( bf.nsId, buffer, value );
  getCacheBuffer( bf, buffer ) = value;
}

const std::string& Storage::get( const std::string& ns, const std::string& buffer )
{
  bool isCachedNs = hasCacheNamespace( ns );
  auto& bf = getCacheNamespace( ns );
  if( !isCachedNs )
  {
    bf.nsId = getNamespaceId( ns );
  }
  if( hasCacheBuffer( bf, buffer ) )
  {
    return getCacheBuffer( bf, buffer );
  }
  const auto db = getBuffer( bf.nsId, buffer );
  auto& value = getCacheBuffer( bf, buffer );
  if( db.has_value() )
  {
    value = std::move( db.value() );
  }
  return value;
}

List Storage::getBufferList( const std::string& ns )
{
  List list;
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "SELECT buff_buffer.name"
                    " FROM buff_buffer, buff_namespace"
                    " WHERE buff_buffer.namespace_id = buff_namespace.id"
                    " AND buff_namespace.name = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_text( stmt, 1, ns.c_str(), ns.size(), SQLITE_STATIC );

  while( ( err = sqlite3_step( stmt ) ) == SQLITE_ROW )
  {
    const auto* value = reinterpret_cast< const char* >( sqlite3_column_text( stmt, 0 ) );
    list.push_back( value );
  }
  // error
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
  return std::move( list );
}

List Storage::getNamespaceList()
{
  List list;
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "SELECT name"
                    " FROM buff_namespace";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  while( ( err = sqlite3_step( stmt ) ) == SQLITE_ROW )
  {
    const auto* value = reinterpret_cast< const char* >( sqlite3_column_text( stmt, 0 ) );
    list.push_back( value );
  }
  // error
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
  return std::move( list );
}

void Storage::removeBuffer( const std::string& ns, const std::string& buffer )
{
  auto nsId = tryGetNamespaceId( ns );
  if( nsId.has_value() )
  {
    removeDbBuffer( nsId.value(), buffer );
    if( hasCacheNamespace( ns ) )
    {
      auto& bf = getCacheNamespace( ns );
      removeCacheBuffer( bf, buffer );
    }
  }
}

void Storage::removeNamespace( const std::string& ns )
{
  auto nsId = tryGetNamespaceId( ns );
  if( nsId.has_value() )
  {
    removeDbBufferAll( nsId.value() );
    removeDbNamespace( ns );
    removeCacheNamespace( ns );
  }
}

void Storage::removeAll()
{
  removeDbAll();
  removeCacheAll();
}

bool Storage::checkBuffer( const std::string& ns, const std::string& buffer )
{
  int nsId;
  bool hasNs = hasCacheNamespace( ns );
  if( hasNs )
  {
    auto& bf = getCacheNamespace( ns );
    if( hasCacheBuffer( bf, buffer ) )
      return true;
    nsId = bf.nsId;
  }
  else
  {
    auto opt = tryGetNamespaceId( ns );
    if( !opt.has_value() )
      return false;
    nsId = opt.value();
  }
  auto opt = getBuffer( nsId, buffer );
  if( !opt.has_value() )
    return false;
  auto& bf = getCacheNamespace( ns );
  if( !hasNs )
    bf.nsId = nsId;
  getCacheBuffer( bf, buffer ) = std::move( opt.value() );
  return true;
}

bool Storage::checkNamespace( const std::string& ns )
{
  return tryGetNamespaceId( ns ).has_value();
}

void Storage::init()
{
  initTableInfo();
  const bool reset = !checkInfoVersion();
  if( reset )
  {
    initInfoVersion();
    initTableNamespace();
    initTableBuffer();
  }
}

void Storage::initTableInfo()
{
  exec( "CREATE TABLE IF NOT EXISTS buff_info("
        " id INTEGER PRIMARY KEY,"
        " name TEXT NOT NULL UNIQUE,"
        " value TEXT NOT NULL )" );
}

void Storage::initTableNamespace()
{
  exec( "DROP TABLE IF EXISTS buff_namespace" );
  exec( "CREATE TABLE IF NOT EXISTS buff_namespace("
        " id INTEGER PRIMARY KEY,"
        " name TEXT NOT NULL UNIQUE )" );
}

void Storage::initTableBuffer()
{
  exec( "DROP TABLE IF EXISTS buff_buffer" );
  exec( "CREATE TABLE IF NOT EXISTS buff_buffer("
        " id INTEGER PRIMARY KEY,"
        " namespace_id INTEGER NOT NULL,"
        " name TEXT NOT NULL,"
        " value BLOB NOT NULL,"
        " UNIQUE( namespace_id, name )"
        " FOREIGN KEY (namespace_id) REFERENCES buff_namespace (id)"
        " ON DELETE CASCADE ON UPDATE NO ACTION )" );
}

void Storage::initInfoVersion()
{
  exec( "INSERT INTO buff_info ( name, value )"
        " VALUES ( 'version', '1' )"
        " ON CONFLICT ( name )"
        " DO UPDATE SET value = excluded.value" );
}

bool Storage::checkInfoVersion()
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "SELECT value"
                    " FROM buff_info"
                    " WHERE name = 'version'";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  err = sqlite3_step( stmt );
  // no rows of version
  if( err == SQLITE_DONE )
  {
    sqlite3_finalize( stmt );
    return false;
  }
  // error
  if( err != SQLITE_ROW )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  // get value
  const auto* value = reinterpret_cast< const char* >( sqlite3_column_text( stmt, 0 ) );
  int version = std::atoi( value );
  sqlite3_finalize( stmt );
  
  return version == m_version;
}

int Storage::getNamespaceId( const std::string& ns )
{
  auto id = tryGetNamespaceId( ns );
  return id.has_value() ? id.value() : insertNamespace( ns );
}

std::optional< int > Storage::tryGetNamespaceId( const std::string& ns )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "SELECT id"
                    " FROM buff_namespace"
                    " WHERE name = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_text( stmt, 1, ns.c_str(), ns.size(), SQLITE_STATIC );

  // perform
  err = sqlite3_step( stmt );
  // no rows
  if( err == SQLITE_DONE )
  {
    sqlite3_finalize( stmt );
    return std::nullopt;
  }
  // error
  if( err != SQLITE_ROW )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  // get value
  const int id = sqlite3_column_int( stmt, 0 );
  sqlite3_finalize( stmt );

  return id;
}

int Storage::insertNamespace( const std::string& ns )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "INSERT INTO buff_namespace ( name )"
                    " VALUES ( ? )";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_text( stmt, 1, ns.c_str(), ns.size(), SQLITE_STATIC );

  // perform
  err = sqlite3_step( stmt );
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );

  return sqlite3_last_insert_rowid( m_db );
}

void Storage::insertBuffer( int ns, const std::string& name, const std::string& value )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "INSERT INTO buff_buffer ( name, value, namespace_id )"
                    " VALUES ( ?, ?, ? )"
                    " ON CONFLICT ( namespace_id, name )"
                    " DO UPDATE SET value = excluded.value";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_text( stmt, 1, name.c_str(), name.size(), SQLITE_STATIC );
  sqlite3_bind_blob( stmt, 2, value.c_str(), value.size(), SQLITE_STATIC );
  sqlite3_bind_int( stmt, 3, ns );

  // perform
  err = sqlite3_step( stmt );
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
}

std::optional< std::string > Storage::getBuffer( int ns, const std::string& name )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "SELECT value"
                    " FROM buff_buffer"
                    " WHERE namespace_id = ? AND name = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_int( stmt, 1, ns );
  sqlite3_bind_text( stmt, 2, name.c_str(), name.size(), SQLITE_STATIC );

  // perform
  err = sqlite3_step( stmt );
  // no rows
  if( err == SQLITE_DONE )
  {
    sqlite3_finalize( stmt );
    return std::nullopt;
  }
  // error
  if( err != SQLITE_ROW )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  // get value
  const auto sz = sqlite3_column_bytes( stmt, 0 );
  const auto* value = static_cast< const char* >( sqlite3_column_blob( stmt, 0 ) );
  std::string result( value, sz );
  sqlite3_finalize( stmt );

  return std::move( result );
}

void Storage::removeDbBuffer( int ns, const std::string& name )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "DELETE FROM buff_buffer"
                    " WHERE namespace_id = ?"
                    " AND name = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_int( stmt, 1, ns );
  sqlite3_bind_text( stmt, 2, name.c_str(), name.size(), SQLITE_STATIC );

  // perform
  err = sqlite3_step( stmt );
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
}

void Storage::removeDbBufferAll( int ns )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "DELETE FROM buff_buffer"
                    " WHERE namespace_id = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_int( stmt, 1, ns );

  // perform
  err = sqlite3_step( stmt );
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
}

void Storage::removeDbNamespace( const std::string& ns )
{
  sqlite3_stmt* stmt;
  int err;
  const char* sql = "DELETE FROM buff_namespace"
                    " WHERE name = ?";

  err = sqlite3_prepare_v2( m_db, sql, -1, &stmt, nullptr );
  if( err != SQLITE_OK )
    throw std::logic_error( sqlite3_errmsg( m_db ) );

  // bind
  sqlite3_bind_text( stmt, 1, ns.c_str(), ns.size(), SQLITE_STATIC );

  // perform
  err = sqlite3_step( stmt );
  if( err != SQLITE_DONE )
  {
    const std::string msg = sqlite3_errmsg( m_db );
    sqlite3_finalize( stmt );
    throw std::logic_error( msg );
  }
  sqlite3_finalize( stmt );
}

void Storage::removeDbAll()
{
  exec( "DELETE FROM buff_buffer" );
  exec( "DELETE FROM buff_namespace" );
}

void Storage::exec( const char* sql )
{
  assert( m_db );
  char* err;
  if( sqlite3_exec( m_db, sql, nullptr, nullptr, &err ) )
  {
    std::string msg( err );
    sqlite3_free( err );
    throw std::logic_error( msg );
  }
}

bool Storage::hasCacheNamespace( const std::string& name )
{
  return m_cache.namespaces.find( name ) != m_cache.namespaces.end();
}

Storage::BuffersCache& Storage::getCacheNamespace( const std::string& name )
{
  if( !hasCacheNamespace( name ) )
  {
    // no exists
    m_cache.namespaces.insert( { name, std::make_shared< BuffersCache >() } );
    m_cache.history.push_front( name );
    if( m_cache.history.size() > m_cacheNamespacesMax )
    {
      m_cache.namespaces.erase( m_cache.history.back() );
      m_cache.history.pop_back();
    }
  }
  else
  {
    // exists
    auto it = std::find( m_cache.history.begin(),
                         m_cache.history.end(),
                         name );
    if( it != m_cache.history.begin() )
      m_cache.history.splice( m_cache.history.begin(),
                              m_cache.history, std::next( it ) );
  }
  return *m_cache.namespaces.at( name );
}

bool Storage::hasCacheBuffer( const BuffersCache& bf, const std::string& name )
{
  return bf.buffers.find( name ) != bf.buffers.end();
}

std::string& Storage::getCacheBuffer( BuffersCache& bf, const std::string& name )
{
  if( !hasCacheBuffer( bf, name ) )
  {
    bf.buffers.insert( { name, "" } );
    bf.history.push_front( name );
    if( bf.history.size() > m_cacheBuffersMax )
    {
      bf.buffers.erase( bf.history.back() );
      bf.history.pop_back();
    }
  }
  else
  {
    auto it = std::find( bf.history.begin(),
                         bf.history.end(),
                         name );
    if( it != bf.history.begin() )
      bf.history.splice( bf.history.begin(),
                         bf.history, std::next( it ) );
  }
  return bf.buffers.at( name );
}

void Storage::removeCacheBuffer( BuffersCache& bf, const std::string& name )
{
  if( hasCacheBuffer( bf, name ) )
  {
    bf.buffers.erase( name );
    auto it = std::find( bf.history.begin(),
                         bf.history.end(),
                         name );
    bf.history.erase( it );
  }
}

void Storage::removeCacheNamespace( const std::string& ns )
{
  if( hasCacheNamespace( ns ) )
  {
    m_cache.namespaces.erase( ns );
    auto it = std::find( m_cache.history.begin(),
                         m_cache.history.end(),
                         ns );
    m_cache.history.erase( it );
  }
}

void Storage::removeCacheAll()
{
  m_cache.namespaces.clear();
  m_cache.history.clear();
}

