#include "Config.h"
#include "tools.h"

#include <yaml-cpp/yaml.h>
#include <iostream>

Config::Config():
  m_defaultValue( "" ),
  m_defaultNamespace( "common" ),
  m_cacheFile( tools::getDefaultCacheFile() )
{
}

Config::~Config()
{
}

void Config::load()
{
  auto filename = tools::getConfigFile();
  if( !filename.has_value() )
    return;

  YAML::Node root = YAML::LoadFile( filename.value() );
  if( !root.IsMap() )
    throw std::logic_error( "Invalid config: root should be map" );

  for( const auto& node : root )
  {
    const auto& name = node.first.Scalar();
    const auto& data = node.second;

    if( name == "default_value" )
    {
      if( !data.IsScalar() )
        throw std::logic_error( "Invalid config: default_value should be scalar" );
      m_defaultValue = data.Scalar();
    }
    else if( name == "default_namespace" )
    {
      if( !data.IsScalar() )
        throw std::logic_error( "Invalid config: default_namespace should be scalar" );
      m_defaultNamespace = data.Scalar();
    }
    else if( name == "cache_file" )
    {
      if( !data.IsScalar() )
        throw std::logic_error( "Invalid config: cache_file should be scalar" );
      m_cacheFile = data.Scalar();
    }
    else
    {
      std::cerr << "Unknown config parameter '" << name << "'" << std::endl;
    }
  }
}

const std::string& Config::getDefaultValue() const
{
  return m_defaultValue;
}

const std::string& Config::getDefaultNamespace() const
{
  return m_defaultNamespace;
}

const std::string& Config::getCacheFile() const
{
  return m_cacheFile;
}

