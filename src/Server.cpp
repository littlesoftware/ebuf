#include "Server.h"
#include "tools.h"
#include "Context.h"

#include "reqrep.pb.h"
#include <zmq.h>

#include <cassert>
#include <exception>

Server::Server():
  m_socket( nullptr ),
  m_listener( nullptr )
{
}

Server::~Server()
{
  close();
}

void Server::bind()
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  close();
  m_socket = zmq_socket( Context::get(), ZMQ_REP );
  assert( m_socket );

  zmq_bind( m_socket, tools::getProtocol().c_str() );
}

void Server::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}

void Server::run()
{
  assert( m_socket );
  reqrep::Request request;
  reqrep::Response response;
  m_request = &request;
  m_response = &response;
  m_running = true;
  while( m_running )
  {
    zmq_msg_t req;
    zmq_msg_init( &req );
    zmq_msg_recv( &req, m_socket, 0 );

    request.ParseFromArray( zmq_msg_data( &req ), zmq_msg_size( &req ) );
    zmq_msg_close( &req );

    try
    {
      switch( request.payload_case() )
      {
      case reqrep::Request::PayloadCase::kKillServer:
        onKillServer();
        break;
      case reqrep::Request::PayloadCase::kSetBuffer:
        onSetBuffer();
        break;
      case reqrep::Request::PayloadCase::kGetBuffer:
        onGetBuffer();
        break;
      case reqrep::Request::PayloadCase::kGetBufferList:
        onGetBufferList();
        break;
      case reqrep::Request::PayloadCase::kGetNamespaceList:
        onGetNamespaceList();
        break;
      case reqrep::Request::PayloadCase::kRemoveBuffer:
        onRemoveBuffer();
        break;
      case reqrep::Request::PayloadCase::kRemoveNamespace:
        onRemoveNamespace();
        break;
      case reqrep::Request::PayloadCase::kRemoveAll:
        onRemoveAll();
        break;
      case reqrep::Request::PayloadCase::kCheckBuffer:
        onCheckBuffer();
        break;
      case reqrep::Request::PayloadCase::kCheckNamespace:
        onCheckNamespace();
        break;
      default:
        makeErrorResponse( tools::format( "Unknown request type %d", request.payload_case() ) );
        break;
      }
    }
    catch( const std::exception& err )
    {
      makeErrorResponse( err.what() );
    }
    catch( ... )
    {
      makeErrorResponse( "Unknown error" );
    }

    // response
    zmq_msg_t resp;
    std::string data;
    response.SerializeToString( &data );
    zmq_msg_init_size( &resp, data.size() );
    memcpy( zmq_msg_data( &resp ), data.c_str(), data.size() );

    zmq_msg_send( &resp, m_socket, 0 );
    zmq_msg_close( &resp );
  }
}

void Server::stop()
{
  m_running = false;
}

void Server::setListener( ServerListener* listener )
{
  m_listener = listener;
}

void Server::setDefaultNamespace( const std::string& ns )
{
  m_defaultNamespace = ns;
}

void Server::setDefaultValue( const std::string& value )
{
  m_defaultValue = value;
}

void Server::makeErrorResponse( const std::string& message )
{
  auto error = m_response->mutable_error();
  error->set_message( message );
}

void Server::makeSuccessResponse()
{
  m_response->mutable_success();
}

void Server::onKillServer()
{
  m_listener->onKillServer();
  makeSuccessResponse();
}

void Server::onSetBuffer()
{
  const auto& info = m_request->setbuffer();
  m_listener->onSetBuffer(
      info.name(),
      info.has_ns() ? info.ns() : m_defaultNamespace,
      info.value() );
  makeSuccessResponse();
}

void Server::onGetBuffer()
{
  const auto& info = m_request->getbuffer();
  auto resp = m_response->mutable_buffer();
  auto value = m_listener->onGetBuffer(
        info.name(),
        info.has_ns() ? info.ns() : m_defaultNamespace );
  if( value.has_value() )
    resp->set_value( std::move( value.value() ) );
  else if ( info.has_defaultvalue() )
    resp->set_value( info.defaultvalue() );
  else
    resp->set_value( m_defaultValue );
}

void Server::onGetBufferList()
{
  const auto& info = m_request->getbufferlist();
  auto list = m_listener->onGetBufferList(
      info.has_ns() ? info.ns() : m_defaultNamespace );
  auto resp = m_response->mutable_list();
  resp->clear_item();
  for( auto& name : list )
  {
    resp->add_item( std::move( name ) );
  }
}

void Server::onGetNamespaceList()
{
  const auto& info = m_request->getnamespacelist();
  auto list = m_listener->onGetNamespaceList();
  auto resp = m_response->mutable_list();
  resp->clear_item();
  for( auto& name : list )
  {
    resp->add_item( std::move( name ) );
  }
}

void Server::onRemoveBuffer()
{
  const auto& info = m_request->removebuffer();
  m_listener->onRemoveBuffer(
      info.name(),
      info.has_ns() ? info.ns() : m_defaultNamespace );
  makeSuccessResponse();
}

void Server::onRemoveNamespace()
{
  const auto& info = m_request->removenamespace();
  m_listener->onRemoveNamespace( info.ns() );
  makeSuccessResponse();
}

void Server::onRemoveAll()
{
  const auto& info = m_request->removeall();
  m_listener->onRemoveAll();
  makeSuccessResponse();
}

void Server::onCheckBuffer()
{
  const auto& info = m_request->checkbuffer();
  auto resp = m_response->mutable_checked();
  resp->set_result(
      m_listener->onCheckBuffer(
        info.name(),
        info.has_ns() ? info.ns() : m_defaultNamespace ) );
}

void Server::onCheckNamespace()
{
  const auto& info = m_request->checknamespace();
  auto resp = m_response->mutable_checked();
  resp->set_result( m_listener->onCheckNamespace( info.ns() ) );
}

