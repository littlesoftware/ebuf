#include "Client.h"
#include "Context.h"
#include "tools.h"

#include "reqrep.pb.h"
#include <zmq.h>

#include <cassert>

Client::Client():
  m_socket( nullptr )
{
}

Client::~Client()
{
  close();
}

void Client::connect()
{
  GOOGLE_PROTOBUF_VERIFY_VERSION;
  close();
  m_socket = zmq_socket( Context::get(), ZMQ_REQ );
  assert( m_socket );

  zmq_connect( m_socket, tools::getProtocol().c_str() );
}

void Client::close()
{
  if( m_socket )
  {
    zmq_close( m_socket );
    m_socket = nullptr;
  }
}

inline void require( const reqrep::Request& request,
                     reqrep::Response& response,
                     const reqrep::Response::PayloadCase type,
                     void* socket )
{
  std::string data;
  request.SerializeToString( &data );
  // send
  zmq_msg_t req;
  zmq_msg_init_size( &req, data.size() );
  memcpy( zmq_msg_data( &req ), data.c_str(), data.size() );

  zmq_msg_send( &req, socket, 0 );
  zmq_msg_close( &req );
  // recv
  zmq_msg_t resp;
  zmq_msg_init( &resp );
  zmq_msg_recv( &resp, socket, 0 );

  response.ParseFromArray( zmq_msg_data( &resp ), zmq_msg_size( &resp ) );
  zmq_msg_close( &resp );

  if( type == response.payload_case() )
    return;
  switch( response.payload_case() )
  {
  case reqrep::Response::PayloadCase::kError:
    throw std::logic_error( response.error().message() );
    break;
  default:
    throw std::logic_error( tools::format( "Unexpected response %d", response.payload_case() ) );
    break;
  }
}

std::string Client::getBuffer( const std::string& name, const OptStr& ns, const OptStr& def )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto getBuffer = request.mutable_getbuffer();
  getBuffer->set_name( name );
  if( ns.has_value() )
    getBuffer->set_ns( ns.value() );
  if( def.has_value() )
    getBuffer->set_defaultvalue( def.value() );

  require( request, response, reqrep::Response::PayloadCase::kBuffer, m_socket );
  return response.buffer().value();
}

void Client::setBuffer( const std::string& name, const OptStr& ns, const std::string& value )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto setBuffer = request.mutable_setbuffer();
  setBuffer->set_name( name );
  if( ns.has_value() )
    setBuffer->set_ns( ns.value() );
  setBuffer->set_value( value );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

void Client::killServer()
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto killServer = request.mutable_killserver();

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

List Client::getBufferList( const OptStr& ns )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto getBufferList = request.mutable_getbufferlist();
  if( ns.has_value() )
    getBufferList->set_ns( ns.value() );

  require( request, response, reqrep::Response::PayloadCase::kList, m_socket );

  List list;
  list.reserve( response.list().item_size() );
  std::copy( response.list().item().begin(),
             response.list().item().end(),
             std::back_inserter( list ) );
  return std::move( list );
}

List Client::getNamespaceList()
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto getNamespaceList = request.mutable_getnamespacelist();

  require( request, response, reqrep::Response::PayloadCase::kList, m_socket );

  List list;
  list.reserve( response.list().item_size() );
  std::copy( response.list().item().begin(),
             response.list().item().end(),
             std::back_inserter( list ) );
  return std::move( list );
}

void Client::removeBuffer( const std::string& name, const OptStr& ns )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto removeBuffer = request.mutable_removebuffer();
  if( ns.has_value() )
    removeBuffer->set_ns( ns.value() );
  removeBuffer->set_name( name );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

void Client::removeNamespace( const std::string& ns )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto removeNamespace = request.mutable_removenamespace();
  removeNamespace->set_ns( ns );

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

void Client::removeAll()
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto removeNamespace = request.mutable_removeall();

  require( request, response, reqrep::Response::PayloadCase::kSuccess, m_socket );
}

bool Client::checkBuffer( const std::string& name, const OptStr& ns )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto checkBuffer = request.mutable_checkbuffer();
  if( ns.has_value() )
    checkBuffer->set_ns( ns.value() );
  checkBuffer->set_name( name );

  require( request, response, reqrep::Response::PayloadCase::kChecked, m_socket );
  return response.checked().result();
}

bool Client::checkNamespace( const std::string& ns )
{
  reqrep::Request request;
  reqrep::Response response;
  //make request
  auto checkNamespace = request.mutable_checknamespace();
  checkNamespace->set_ns( ns );

  require( request, response, reqrep::Response::PayloadCase::kChecked, m_socket );
  return response.checked().result();
}

