#pragma once

#include "ServerListener.h"
#include "Server.h"
#include "Storage.h"
#include "Config.h"

class Manager: public ServerListener
{
public:
  Manager();
  virtual ~Manager();

  void runServer();

  virtual void onKillServer() override;
  virtual OptStr onGetBuffer( const std::string& name, const std::string& ns ) override;
  virtual void onSetBuffer( const std::string& name,
                            const std::string& ns,
                            const std::string& value ) override;
  virtual List onGetBufferList( const std::string& ns ) override;
  virtual List onGetNamespaceList() override;
  virtual void onRemoveBuffer( const std::string& name, const std::string& ns ) override;
  virtual void onRemoveNamespace( const std::string& ns ) override;
  virtual void onRemoveAll() override;
  virtual bool onCheckBuffer( const std::string& name, const std::string& ns ) override;
  virtual bool onCheckNamespace( const std::string& ns ) override;

private:
  Server m_server;
  Storage m_storage;
  Config m_config;
};
