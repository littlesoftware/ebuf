#pragma once

#include "types.h"

#include <string>
#include <list>
#include <optional>

class Handler
{
public:
  enum class Action
  {
    Unknown,
    Help,
    Version,
    Yank,
    Paste,
    Kill,
    RemoveBuffer,
    RemoveNamespace,
    RemoveAll,
    CheckBuffer,
    CheckNamespace,
    BufferList,
    NamespaceList
  };

  Handler();
  ~Handler();

  static int execute( int argc, char** argv );
  static Handler& instance();

  void parseOptions( int argc, char** argv );
  Action getAction() const;

private:
  void onHelp();
  void onVersion();
  void onKill();
  void onYank();
  void onPaste();
  void onBufferList();
  void onNamespaceList();
  void onRemoveBuffer();
  void onRemoveNamespace();
  void onRemoveAll();
  void onCheckBuffer();
  void onCheckNamespace();
  void makeServer();
  static void onRunServer();

private:
  Action m_action;
  std::string m_buffer;
  OptStr m_namespace;
  OptStr m_defaultValue;
  std::list< std::string > m_args;
};
