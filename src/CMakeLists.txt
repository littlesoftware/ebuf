cmake_minimum_required( VERSION 3.12 )
project( src )

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_CXX_STANDARD_REQUIRED ON )

file( GLOB_RECURSE SOURCES "*.cpp" )

add_library( ${PROJECT_NAME} STATIC ${SOURCES} )
target_link_libraries( ${PROJECT_NAME} pthread proto zmq sqlite3 yaml-cpp )

