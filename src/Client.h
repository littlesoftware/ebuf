#pragma once

#include "types.h"

#include <string>
#include <optional>

class Client
{
public:
  Client();
  ~Client();

  void connect();
  void close();

  std::string getBuffer( const std::string& name, const OptStr& ns, const OptStr& def );
  void setBuffer( const std::string& name, const OptStr& ns, const std::string& value );
  void killServer();
  List getBufferList( const OptStr& ns );
  List getNamespaceList();
  void removeBuffer( const std::string& name, const OptStr& ns );
  void removeNamespace( const std::string& ns );
  void removeAll();
  bool checkBuffer( const std::string& name, const OptStr& ns );
  bool checkNamespace( const std::string& ns );

private:
  void* m_socket;
};
