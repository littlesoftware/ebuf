#pragma once

#include "types.h"

#include <string>

class ServerListener
{
public:
  virtual ~ServerListener() = default;

  virtual void onKillServer() = 0;
  virtual OptStr onGetBuffer( const std::string& name, const std::string& ns ) = 0;
  virtual void onSetBuffer( const std::string& name,
                            const std::string& ns,
                            const std::string& value ) = 0;
  virtual List onGetBufferList( const std::string& ns ) = 0;
  virtual List onGetNamespaceList() = 0;
  virtual void onRemoveBuffer( const std::string& name, const std::string& ns ) = 0;
  virtual void onRemoveNamespace( const std::string& ns ) = 0;
  virtual void onRemoveAll() = 0;
  virtual bool onCheckBuffer( const std::string& name, const std::string& ns ) = 0;
  virtual bool onCheckNamespace( const std::string& ns ) = 0;
};
