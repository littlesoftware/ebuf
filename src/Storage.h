#pragma once

#include "types.h"

#include <map>
#include <string>
#include <optional>
#include <memory>
#include <list>

struct sqlite3;

class Storage
{
public:
  Storage();
  ~Storage();

  void open( const std::string& filename );
  void close();
  void set( const std::string& ns, const std::string& buffer, const std::string& value );
  const std::string& get( const std::string& ns, const std::string& buffer );
  List getBufferList( const std::string& ns );
  List getNamespaceList();
  void removeBuffer( const std::string& ns, const std::string& buffer );
  void removeNamespace( const std::string& ns );
  void removeAll();
  bool checkBuffer( const std::string& ns, const std::string& buffer );
  bool checkNamespace( const std::string& ns );

private:
  using History = std::list< std::string >;
  using Buffers = std::map< std::string, std::string >;

  struct BuffersCache {
    Buffers buffers;
    History history;
    int nsId;
  };

  using Namespaces = std::map< std::string, std::shared_ptr< BuffersCache > >;

  struct NamespacesCache {
    Namespaces namespaces;
    History history;
  };

  NamespacesCache m_cache;
  sqlite3* m_db;
  const int m_version = 1;
  int m_cacheNamespacesMax = 10;
  int m_cacheBuffersMax = 100;

private:
  inline void init();
  inline void initTableInfo();
  inline void initTableNamespace();
  inline void initTableBuffer();
  inline void initInfoVersion();
  inline bool checkInfoVersion();
  inline int getNamespaceId( const std::string& ns );
  inline std::optional< int > tryGetNamespaceId( const std::string& ns );
  inline int insertNamespace( const std::string& ns );
  inline void insertBuffer( int ns, const std::string& name, const std::string& value );
  inline std::optional< std::string > getBuffer( int ns, const std::string& name );
  inline void removeDbBuffer( int ns, const std::string& name );
  inline void removeDbBufferAll( int ns );
  inline void removeDbNamespace( const std::string& ns );
  inline void removeDbAll();
  inline void exec( const char* sql );
  inline bool hasCacheNamespace( const std::string& name );
  inline BuffersCache& getCacheNamespace( const std::string& name );
  inline bool hasCacheBuffer( const BuffersCache& bf, const std::string& name );
  inline std::string& getCacheBuffer( BuffersCache& bf, const std::string& name );
  inline void removeCacheBuffer( BuffersCache& bf, const std::string& name );
  inline void removeCacheNamespace( const std::string& ns );
  inline void removeCacheAll();
};
